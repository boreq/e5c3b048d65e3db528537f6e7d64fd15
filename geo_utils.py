import math


# EPSG:4326 http://spatialreference.org/ref/epsg/wgs-84/
# EPSG:3857 http://spatialreference.org/ref/sr-org/7483/
earth_radius = 6378137 


def coords_to_mercator(lon, lat):
    """Converts EPSG:4326 coords to EPSG:3857."""
    if abs(lat) > 85:
        raise ValueError('Lat too large')
    y = earth_radius * math.log(math.tan(math.pi / 4 + math.radians(lat) / 2))
    x = earth_radius * math.radians(lon)
    return x, y


def mercator_to_coords(x, y):
    """Converts EPSG:3857 coords to EPSG:4326."""
    lat = 2 * (math.atan(math.pow(math.e, y / earth_radius)) - math.pi / 4)
    lon = x / earth_radius
    return math.degrees(lon), math.degrees(lat)


def mercator_to_tile(x, y, n):
    """Converts EPSG:3857 coordinates to a tile number. [0, 0] is the top left
    tile and [n - 1, n - 1] is the bottom right tile.
    
    x, y: EPSG:3857 coordinates.
    n: number of tiles per map width or height (for example n=16 means that
       there are 256 tiles in total).
    """
    origin = [-20037508.342789244, 20037508.342789244]
    tile_size = 2 * origin[1] / n
    x -= origin[0]
    y -= origin[1]
    tileX = x / tile_size
    tileY = y / tile_size
    return math.floor(tileX), math.floor(-tileY)


def bearing(lon1, lat1, lon2, lat2):
    """Calculates an initial bearing in degrees between two coordinates.""" 
    lon1 = math.radians(lon1)
    lat1 = math.radians(lat1)
    lon2 = math.radians(lon2)
    lat2 = math.radians(lat2)

    y = math.sin(lon2 - lon1) * math.cos(lat2)
    x = math.cos(lat1) * math.sin(lat2) - \
        math.sin(lat1) * math.cos(lat2) * math.cos(lon2 - lon1)
    bearing = math.atan2(y, x)
    return math.degrees(bearing)


def distance(lon1, lat1, lon2, lat2):
    """Calculates a distance in kilometers between two coordinates.""" 
    lon1 = math.radians(lon1)
    lat1 = math.radians(lat1)
    lon2 = math.radians(lon2)
    lat2 = math.radians(lat2)

    p1 = pow(math.sin((lat2 - lat1) / 2), 2)
    p2 = pow(math.sin((lon2 - lon1) / 2), 2)
    a =  p1 + math.cos(lat1) * math.cos(lat2) * p2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    return c * 6371